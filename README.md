# LoadSaver4ZIP

Loads and saves ZIP files from filesystem with JSZip and FileSaver.js. LoadSaver4ZIP allows the modification of zip-files like JSzip and extends the features JSZip for updating and adding zip-files of e.g. LibreOffice or Geogebra docs.